# -*- coding: utf-8 -*-

import pandas as pd
import json
import datetime
import emabinance as ema
import twitterscript as twitter
from binance.client import Client
import json
from datetime import datetime
import calendar

#Definición de keys para ingresar a API
Authl = 'zs8eqgfKmrV2MTy1AEyxDCaz5Gud3xivYjz8G40krh3DtDdE0RKKl445GqbMg45F'
Auth2 = 'Ja152yaY88dcHm7jHYyzmvm7ClyYlZtiUn7IrtsJRIe6lT9fcKGNe8VYz7I1CXAi'


#Conexión a la API de binance con data estructurada. Fuente de datos exhaustiva.
client = Client(Authl,Auth2)

#Definición del DataFrame y extracción de la criptomoneda que se desea estudiar con intervalos diarios y últimos 90 días. 
klines = pd.DataFrame(client.get_historical_klines("DOGEUSDT", Client.KLINE_INTERVAL_1DAY,"90 day ago UTC"))

##########
data_history = client.get_historical_klines("DOGEUSDT", Client.KLINE_INTERVAL_1DAY,"90 day ago UTC")

candles = []

for x in range(len(data_history)): 
    candles.append({
        'date': data_history[x][0],
        'Low': float(data_history[x][1]),
        'High': float(data_history[x][2]),
        'Open': float(data_history[x][3]),
        'Close': float(data_history[x][4]),
        'Volume': float(data_history[x][5]),
        'Closetime': str(data_history[x][6])
        })

candles = ema.start(candles)


klines = pd.DataFrame(candles)
klines.columns = ["date","Open","High","Low","Close","Volume","Closetime","Sma","Ema"]
##########


##klines.columns = ["date","Open","High","Low","Close","Volume","Closetime","Quote_asset_volume","Number_of_trades","Taker_buy_base_asset_volume","Taker_buy_quote_asset_volume","Ignore"]
#LAS COLUMNAS QUE QUEDAN SON: 
#Opentime: Apertura de la vela.
#Open: Valor con que abrio la criptomoneda.
#High: Mayor valor dentro del periodo.
#Low: Menor valor dentro del periodo.
#Close: Valor con que cerro la criptomoneda.

#Eliminación de las columnas que no utilizaremos
##klines.drop(['Volume','Quote_asset_volume','Taker_buy_base_asset_volume','Taker_buy_quote_asset_volume','Taker_buy_base_asset_volume','Ignore','Closetime'], axis='columns', inplace=True)

#Cambio de formato de columnas de tiempo desde timestamp a datatime
klines['date']=pd.to_datetime(klines['date'], unit = 'ms')

#Cambiando tipo de datos 
klines['Open'] = klines['Open'].astype(float)
klines['High'] = klines['High'].astype(float)
klines['Low'] = klines['Low'].astype(float)
klines['Close'] = klines['Close'].astype(float)
klines['Volume'] = klines['Volume'].astype(float)
klines['date'] = klines['date'].astype(str)
klines['Closetime'] = klines['Closetime'].astype(str)


#Calcular la diferencia en el valor entre dias de trades.
#klines['var_trades'] = klines['Number_of_trades'].diff(periods = 1)
#klines['var%_trades'] = klines['Number_of_trades'].pct_change(periods = 1)

klines.head(5)
#Se definen las columnas index para hacer el join
klines = klines.set_index('date')

#llamamos a el script twitter para hacer los calculos 
tweets_df_fin = twitter.twiterFunction()

Cointweet = klines.join(tweets_df_fin, lsuffix='date', rsuffix='date')
Cointweet.Closetime = Cointweet.Closetime.apply(lambda d: datetime.utcfromtimestamp( int(d[:-3])).strftime('%Y-%m-%d'))
Cointweet.Closetime= Cointweet.Closetime.astype(str)

#Creación de respaldo de df anterior en csv
#Cointweet.to_csv('Cointweet.csv', header=True, mode = 'w', sep=';')

#cambiar tipo de dataframe a json
Cointweet = Cointweet.to_json()

#Crear JSON--> me imprime pero no tiene el formato correcto de fechas!!
with open('Cointweet.json','w') as json_file:
  json.dump(Cointweet, json_file, indent = 4,sort_keys=True)


json_object = json.loads(Cointweet)
json_formatted_str = json.dumps(json_object, indent=4)
print(json_formatted_str)


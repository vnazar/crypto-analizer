"""CODIGO TWEETER"""

#Importar Librería
import tweepy
import pandas as pd

consumer_key = "fSaYPHzRt3xuYP12YspEUxZhz"
consumer_secret = "j76Dw75hWhbV8dPbxCah1UFr3VdCzbZ50DBOp8mWY2nj6mtwB4"
access_key = "1389730712547434500-Tlt7SZh5AYKAm2hD4Np2TfZpb4iTZh"
access_secret = "S8Pdok4kzXBZIK3gLLbKsvrX20W88mVeJMejrbYXLyNa6"


def twiterFunction():

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    api = tweepy.API(auth, wait_on_rate_limit=True)
    # Extracción Tweets de la cuenta @elenmusk
    tweets = api.user_timeline(screen_name = "elonmusk", count = 100)

    # Consolidación Tweets
    tweets_list = [[tweet.created_at, tweet.user.screen_name, tweet.text, tweet.id_str] for tweet in tweets]
    tweets_df = pd.DataFrame(data=tweets_list, columns=['date', 'user', 'text', 'id_tweet'])

    #Identificación de id_tweet menor para continuar la extracción
    tweets_df["id_tweet"] = tweets_df["id_tweet"].astype('int64')
    nrows = len(tweets_df.index)-1
    min_id = tweets_df["id_tweet"][nrows]

    #Extracción de año, mes y día. Identificación de año de tweet más antiguo
    tweets_df['Year'] = tweets_df['date'].dt.year 
    tweets_df['Month'] = tweets_df['date'].dt.month
    tweets_df['Day'] = tweets_df['date'].dt.day 
    min_year = tweets_df['Year'][nrows]
    print("loading data twitter....")
    while min_year == 2021:
        #La variable min_id identífca el último Tweet extraido y el ciclo extrae lo 100 Tweets anterior a ese
        tweets = api.user_timeline(screen_name = "elonmusk", count = 100, max_id = min_id)
        #Consolidación de Tweets
        tweets_list = [[tweet.created_at, tweet.user.screen_name, tweet.text, tweet.id_str] for tweet in tweets]
        tweets_df_2 = pd.DataFrame(data=tweets_list, columns=['date', 'user', 'text', 'id_tweet'])
        nrows = len(tweets_df_2.index)-1
  
        #Detección de error: si el último ciclo no logra conectar a la api, el número de filas es 0, por lo que ingresa nuevamente la condición inicial del ciclo while
        if nrows <= 0:
            min_year = 2021
        else:
            #Identificación de id_tweet menor para continuar la extracción
            tweets_df_2["id_tweet"] = tweets_df_2["id_tweet"].astype('int64')
            min_id = tweets_df_2["id_tweet"][nrows]
            #Extracción de año, mes y día. Identificación de año de tweet más antiguo
            tweets_df_2['Year'] = tweets_df_2['date'].dt.year
            tweets_df_2['Month'] = tweets_df_2['date'].dt.month
            tweets_df_2['Day'] = tweets_df_2['date'].dt.day 
            min_year = tweets_df_2['Year'][nrows]
    
            #Consolidación y anexión de Tweets extraidos en el ciclo al primer df extraído
            tweets_df = tweets_df.append(tweets_df_2, ignore_index=True)
    
    tweets_df['text'] = tweets_df['text'].str.lower()
    tweets_df = tweets_df.replace('\n',' ', regex=True) 
    pd.set_option('display.max_columns', None)
    tweets_df.head(5)
    #Creación de variable DumDoge: asinga 1 si el Tweet contiene la palabra "doge". En caso contrario asigna 0
    tweets_df['DumDoge'] = tweets_df['text'].str.find('doge')
    tweets_df['DumDoge'] = [1 if s > 0 else 0 for s in tweets_df['DumDoge']] 

    tweets_df['date'] = tweets_df['date'].dt.date 
    tweets_df_fin = tweets_df.groupby(['date']).max()

    #Eliminando las columnas que no utilizaremos
    tweets_df_fin.drop(['user','text','id_tweet','Year','Month','Day'], axis='columns', inplace=True)

    #se saca de indice la columna date y se cambia a formato datetime para hacer posteriormente el join
    tweets_df_fin.reset_index(drop=False, inplace=True)
    tweets_df_fin["date"]= pd.to_datetime(tweets_df_fin["date"])
    #print de tabla criptomoneda
    pd.set_option('display.max_columns', None)

    #print de tabla tweeter
    pd.set_option('display.max_columns', None)
    tweets_df_fin.head(5)
    print("fin twitter api")
    return tweets_df_fin
   
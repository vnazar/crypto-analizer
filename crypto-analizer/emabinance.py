import csv
import re
from functools import reduce
from dateutil import parser

EMA_LENGTH = 2
EMA_SOURCE = 'Close'

candles = []

def calculate_sma(candles, source):
    length = len(candles)
    sum = reduce((lambda last, x: { source: last[source] + x[source] }), candles)
    sma = sum[source] / length
    return sma


def calculate_ema(candles, source):
    length = len(candles)
    target = candles[0]
    previous = candles[1]

    if 'ema' not in previous or previous['ema'] == None:
        return calculate_sma(candles, source)

    else:
        multiplier = 2 / (length + 1)
        ema = (target[source] * multiplier) + (previous['ema'] * (1 - multiplier))

        return ema

def calculate(candles, source):
    candles[0]['sma'] = calculate_sma(candles, source)
    candles[0]['ema'] = calculate_ema(candles, source)
    
def start(candle):
    candles = candle
    position = 0
    while position + EMA_LENGTH <= len(candles):
        current_candles = candles[position:(position+EMA_LENGTH)]
        current_candles = list(reversed(current_candles))
        calculate(current_candles, EMA_SOURCE)
        position += 1
    return  candles  

